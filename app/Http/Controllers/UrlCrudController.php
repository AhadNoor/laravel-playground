<?php

namespace App\Http\Controllers;

use App\Models\BaseDomain;
use Illuminate\Http\Request;

class UrlCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(BaseDomain $todo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(BaseDomain $todo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, BaseDomain $todo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(BaseDomain $todo)
    {
        //
    }
}
